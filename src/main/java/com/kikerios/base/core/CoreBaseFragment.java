package com.kikerios.base.core;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by kikerios on 12/02/16.
 */
public abstract class CoreBaseFragment extends Fragment {

    protected final String TAG = getDebugTag();
    protected CoreBaseActivity baseActivity;
    protected boolean canResume = false; //Use after pause/stop
    private Unbinder unbinder;

    /**
     * This event fires 1st, before creation of fragment or any views
     * The onAttach method is called when the Fragment instance is associated with an Activity.
     * This does not mean the Activity is fully initialized.
     **/
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Logger.d(TAG, "onAttach has been called. " + context);

        if (context instanceof Activity){
            this.baseActivity = (CoreBaseActivity) context;
        }
    }

    // This event fires 2nd, before views are created for the fragment
    // The onCreate method is called when the Fragment instance is being created, or re-created.
    // Use onCreate for any standard setup that does not require the activity to be fully created
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Logger.d(TAG, "onCreate has been called.");
    }

    // The onCreateView method is called when Fragment should create its View object hierarchy,
    // either dynamically or via XML layout inflation.
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        Logger.d(TAG, "onCreateView has been called.");
        if (getLayoutResourceID() <= 0) {
            throw new RuntimeException(TAG + " Please change view resource referense");
        }
        if (ButterKnifeFragment.existSource(getActivity(), this) != null)
            return ButterKnifeFragment.existSource(getActivity(), this);
        return inflater.inflate(getLayoutResourceID(), parent, false);
    }

    // This event is triggered soon after onCreateView().
    // onViewCreated() is only called if the view returned from onCreateView() is non-null.
    // Any view setup should occur here.  E.g., view lookups and attaching view listeners.
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnifeFragment.bind(getActivity(), this, view);
        Logger.d(TAG, "onViewCreated has been called. savedInstanceState " + savedInstanceState);
        initFragmentView(view, savedInstanceState);
    }

    // This method is called after the parent Activity's onCreate() method has completed.
    // Accessing the view hierarchy of the parent activity must be done in the onActivityCreated.
    // At this point, it is safe to search for activity View objects by their ID, for example.
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Logger.d(TAG, "onActivityCreated has been called.");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Logger.d(TAG, "onSaveInstanceState has been called.");
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        Logger.d(TAG, "onDestroyView has been called.");
    }

    @Override
    public void onPause() {
        super.onPause();
        Logger.d(TAG, "onPause");
        canResume = true;
    }

    @Override
    public void onStop() {
        super.onStop();
        Logger.d(TAG, "onStop");
        canResume = true;
    }

    @Override
    public void onResume() {
        super.onResume();
        Logger.d(TAG, "onResume");
    }

    //layout id
    public abstract int getLayoutResourceID();

    // Setup any handles to view objects here
    public abstract void initFragmentView(View view, Bundle savedInstanceState);

    //return CurrenFragment.class.getSimpleName();
    public abstract String getDebugTag();


}

package com.kikerios.base.core;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.view.View;

import java.util.LinkedHashMap;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by kikerios on 31/01/17.
 * ./com.jakewharton/butterknife/7.0.1/ad53343e57ae0969aaf451a19eef381abe2ad3a1/butterknife-7.0.1-sources.jar!/butterknife/ButterKnife.java
 */

public class ButterKnifeFragment {

    static final Map<String, View> BINDERS_VIEWS = new LinkedHashMap<>();
    private static final String TAG = ButterKnifeFragment.class.getSimpleName();

    public static String getKey(Activity activity, Fragment fragment) {

        return activity.getClass() + ":" + fragment.getClass() + ":" + Integer.toHexString(System.identityHashCode(fragment));

    }

    public static View existSource(Activity activity, Fragment fragment) {
        return BINDERS_VIEWS.get(getKey(activity, fragment));
    }

    static Unbinder bind(Activity activity, Fragment fragment, View source) {

        Logger.log(TAG, "fkey" + getKey(activity, fragment));

        if (existSource(activity, fragment) == null) {
            BINDERS_VIEWS.put( getKey(activity, fragment), source);
        }

         return ButterKnife.bind(fragment, source);

    }
}
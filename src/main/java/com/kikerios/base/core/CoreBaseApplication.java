package com.kikerios.base.core;

import android.content.Context;
import android.support.multidex.MultiDexApplication;

import butterknife.ButterKnife;

/**
 * Created by kikerios on 10/03/16.
 */
public class CoreBaseApplication extends MultiDexApplication {

    private static String TAG = CoreBaseApplication.class.getSimpleName();

    @Override
    public void onCreate() {
        super.onCreate();
        ButterKnife.setDebug(true);
    }

}

package com.kikerios.base.core;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;

import com.kikerios.base.core.interfaces.ApplicationFocusStatus;

import butterknife.BindBool;
import butterknife.ButterKnife;


/**
 * Created by kikerios on 7/03/16.
 */
public abstract class CoreBaseActivity extends AppCompatActivity implements ApplicationFocusStatus {

    protected final String TAG = getDebugTag();
    protected boolean canResume = false;

    /*
     * foreground|background
     */
    public static boolean isAppWentToBg = false;
    public static boolean isWindowFocused = false;
    public static boolean isBackPressed = false;

    protected ViewGroup root;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    /*
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {

       DeviceConfiguration();

        super.onCreate(savedInstanceState);
        Logger.d(TAG, "onCreate has been called.");

        windowFocusChanged(getCurrentActivity());
        setContentView(getLayoutResourceID(), savedInstanceState);

    }

    /*
     * @param layoutResID
     * @param savedInstanceState
     */
    public void setContentView(int layoutResID, Bundle savedInstanceState) {
        Logger.d(TAG, "setContentView has been called.");

        super.setContentView(layoutResID);
        ButterKnife.bind(this);

        try {
            root = (ViewGroup) ((ViewGroup) this.findViewById(android.R.id.content)).getChildAt(0);
        } catch (Exception e) {
            e.printStackTrace();
        }

        initActivityView(savedInstanceState);
    }

    public ViewGroup getRoot() {
        return root;
    }

    @SuppressLint("WrongConstant")
    protected void DeviceConfiguration() {
        Logger.d(TAG, "DeviceConfiguration has been called.");
        if(isTablet()){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Logger.d(TAG, "onCreateOptionsMenu has been called. menu " + menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Logger.d(TAG, "onOptionsItemSelected has been called. item " + item);
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        /**
         * foreground|background
         **/
        if (!isMainClass() ) {
            isBackPressed = true;
        }

        /**
         * foreground|background
         **/
        Logger.d(TAG, "onBackPressed " + isBackPressed + " " + this.getLocalClassName() );

        super.onBackPressed();
    }

    @Override
    public void onStateNotSaved() {
        super.onStateNotSaved();
        Logger.d(TAG, "onStateNotSaved has been called.");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Logger.d(TAG, "onSaveInstanceState has been called.");
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Logger.d(TAG, "onRestoreInstanceState has been called.");
    }

    @Override
    protected void onStart(){
        super.onStart();
        Logger.d(TAG, "onStart has been called.");

        /*
         * foreground|background
         */
        Logger.d(TAG, "onStart isAppWentToBg " + isAppWentToBg);
        ApplicationWillEnterForeground();
    }

    @Override
    protected void onRestart(){
        super.onRestart();
        Logger.d(TAG, "onRestart has been called.");
    }

    @Override
    protected void onResume(){
        super.onResume();
        Logger.d(TAG, "onResume has been called.");
    }

    @Override
    protected void onPause(){
        super.onPause();
        Logger.d(TAG, "onPause has been called.");

        /**
         * resume
         **/
        canResume = true;
    }

    @Override
    protected void onStop(){
        super.onStop();
        Logger.d(TAG, "onStop has been called.");

        /**
         * resume
         **/
        canResume = true;

        /*
         * foreground|background
         */
        ApplicationDidEnterBackground();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        Logger.d(TAG, "onDestroy has been called.");

        if( !isMainClass() && !isSplashClass() ) {
            try {
                Logger.d(TAG, "System.gc has been called.");
                System.gc();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Logger.d(TAG, "onActivityResult has been called.");
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Logger.d(TAG, "onLowMemory has been called.");
    }

    /*
     * @param hasFocus
     */
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        Logger.d(TAG, "onWindowFocusChanged has been called. hasFocus " + hasFocus);

        if (hasFocus) {
            windowFocusChanged(getCurrentActivity());
        }

        /*
         * foreground|background
         */
        isWindowFocused = hasFocus;
        if (isBackPressed && !hasFocus) {
            isBackPressed = false;
            isWindowFocused = true;
        }

    }

    /**
     * @param intent
     **/
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Logger.i(TAG, "onNewIntent " + intent);
    }

    /**
     * @return
     **/
    protected CoreBaseActivity getCurrentActivity() {
        return CoreBaseActivity.this;
    }

    /**
     * foreground|background
     **/
    public void AppIsInForeground() {
        Logger.i(TAG, "App is in foreground");
    }

    /**
     * foreground|background
     **/
    public void AppIsGoingToBackground() {
        Logger.i(TAG, "App is Going to Background");
    }

    /**
     * foreground|background
     **/
    private void ApplicationWillEnterForeground() {
        if (isAppWentToBg) {
            isAppWentToBg = false;

            AppIsInForeground();
        }
    }

    /**
     * foreground|background
     **/
    public void ApplicationDidEnterBackground() {
        if (!isWindowFocused) {
            isAppWentToBg = true;
            AppIsGoingToBackground();
        }
    }

    /**
     * abstract
     **/
    public abstract void initActivityView(Bundle savedInstanceState);
    public abstract String getDebugTag();
    public abstract int getLayoutResourceID();
    public abstract boolean isMainClass();
    public abstract boolean isSplashClass();
    public abstract void windowFocusChanged(Activity activity);
    public abstract boolean isTablet();

}

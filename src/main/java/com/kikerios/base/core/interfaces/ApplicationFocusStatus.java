package com.kikerios.base.core.interfaces;

/**
 * Created by kikerios on 28/07/15.
 */
public interface ApplicationFocusStatus {
    public void AppIsInForeground();
    public void AppIsGoingToBackground();
}
